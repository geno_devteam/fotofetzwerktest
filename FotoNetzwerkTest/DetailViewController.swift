//
//  DetailViewController.swift
//  FotoNetzwerkTest
//
//  Created by Antonio Krampe on 03.09.19.
//  Copyright © 2019 Antonio Krampe. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var imgBild: UIImageView!

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.timestamp!.description
            }
            if let imgBild = imgBild,
               let imgData = detail.image {
                imgBild.image = UIImage(data: imgData)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: Event? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

